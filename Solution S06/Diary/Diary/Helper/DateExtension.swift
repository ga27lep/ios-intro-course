//
//  DateExtension.swift
//  Diary
//
//  Created by Dominic Henze on 10.10.19.
//  Copyright © 2019 TUM LS1. All rights reserved.
//

import Foundation

extension Date {
    static var yesterday: Date {
        return Date().dayBefore
    }
    static var tomorrow: Date {
        return Date().dayAfter
    }
    var dayBefore: Date {
        guard let date = Calendar.current.date(byAdding: .day, value: -1, to: noon) else {
            return Date()
        }
        return date
    }
    var dayAfter: Date {
        guard let date = Calendar.current.date(byAdding: .day, value: 1, to: noon) else {
            return Date()
        }
        return date
    }
    var noon: Date {
        guard let date = Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self) else {
            return Date()
        }
        return date
    }
    
    func daysBefore(amount: Int) -> Date {
        guard let date = Calendar.current.date(byAdding: .day, value: -amount, to: noon) else {
            return Date()
        }
        return date
    }
}
