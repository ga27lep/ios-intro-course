//
//  EditDiary.swift
//  Diary
//
//  Created by ios on 14.10.19.
//  Copyright © 2019 TUM LS1. All rights reserved.
//

import SwiftUI

// MARK: EditDiary
struct EditDiary: View {
    
    // MARK: Stored Properties
    @EnvironmentObject private var model: Model
    
    @State private var description = ""
    @State var selectedMood = 0
    @State var id: DiaryEntry.ID?
    @State private var date = Date()
    @State var mood = DiaryEntry.Mood.happy
    @Environment(\.presentationMode) private var presentationMode
    
    // MARK: Computed Properties
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("New Diary description")) {
                    TextField("Enter description", text: $description)
                }
                Section(header: Text("Moods")) {
                    Picker(selection: $selectedMood, label: Text("Moods")) {
                        ForEach(DiaryEntry.Mood.allCases) { mood in
                            Text("\(self.idToEmoji(moodId: mood.id))")
                        }
                    }.pickerStyle(SegmentedPickerStyle())
                }
            }
            .onAppear(perform: updateStates)
            .onDisappear(perform: save)
            .navigationBarTitle("Mood Editor")
            .navigationBarItems(trailing: saveButton)
        }
    }
    
    private var saveButton: some View {
        Button(action: {
            self.save()
        }) {
            Text("Save").bold()
        }.disabled(description.isEmpty)
    }
    
    func idToEmoji(moodId: Int) -> String {
        switch moodId {
        case 0:
            return "😁"
        case 1:
            return "😢"
        case 2:
            return "🙂"
        case 3:
            return "😌"
        default:
            return "😡"
        }
    }
    func save() {
        guard let id = id, var diary = model.diary(id) else {
            let diary = DiaryEntry(id: nil,
                                   mood: DiaryEntry.Mood(value: selectedMood),
                                   description: TextFormatter().string(for: description) ?? "",
                                   date: date)
            self.id = diary.id
            self.model.save(diary)
            updateStates()
            return
        }
        
        diary.description = TextFormatter().string(for: description) ?? ""
        diary.mood = DiaryEntry.Mood(value: selectedMood)
        self.model.save(diary)
        updateStates()
    }
    private func updateStates() {
        guard let diary = model.diary() else {
            self.mood = DiaryEntry.Mood.happy
            self.description = ""
            return
        }
        
        self.selectedMood = diary.mood.rawValue
        self.date = diary.date
        self.description = diary.description
        self.id = diary.id
      //  self.model.save(diary)
    }
    struct EditDiary_Previews: PreviewProvider {
        static var previews: some View {
            EditDiary().environmentObject(Model.mock)
        }
    }
}
