//
//  DiaryCell.swift
//  Diary
//
//  Created by ios on 12.10.19.
//  Copyright © 2019 TUM LS1. All rights reserved.
//

import SwiftUI

// MARK: Diary Cell
struct DiaryCell: View {
    @State var diary: DiaryEntry
    
    // MARK: Computed Properties
    var body: some View {
        VStack(alignment: .leading, spacing: 2) {
            HStack {
                Text(diary.dayToString)
                Spacer()
                Text(diary.dateFormatted)
                    .font(Font.system(size: 25, weight: .bold))
            }.font(Font.system(size: 35, weight: .bold))
            HStack(alignment: .center, spacing: 80) {
                Text("\(diary.emojiMood)")
                    .font(Font.system(size: 100))
                Text(diary.description)
            }.font(Font.system(size: 14, weight: .regular))
        }
        .padding()
        .cardButtonViewModifier(color: diary.color)
    }
}

struct DiaryCell_Previews: PreviewProvider {
    static var diary = DiaryEntry(mood: .relieved, description: "hello blablabla")
    static var previews: some View {
        DiaryCell(diary: diary)
            .previewLayout(.sizeThatFits)
    }
}
