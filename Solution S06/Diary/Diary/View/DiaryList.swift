//
//  DiaryList.swift
//  Diary
//
//  Created by ios on 12.10.19.
//  Copyright © 2019 TUM LS1. All rights reserved.
//

import SwiftUI

// MARK: DiaryList: a preview of a list of diary entries
struct DiaryList: View {
    @EnvironmentObject var model: Model
    
    var body: some View {
        NavigationView {
            List(model.diaries) { diary in
                DiaryCell(diary: diary)
            }
        }
    }
}

struct DiaryList_Previews: PreviewProvider {
    static var previews: some View {
        DiaryList().environmentObject(Model.mock)
    }
}
