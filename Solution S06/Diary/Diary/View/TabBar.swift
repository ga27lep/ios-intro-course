//
//  TabBar.swift
//  Diary
//
//  Created by Dominic Henze on 09.10.19.
//  Copyright © 2019 TUM LS1. All rights reserved.
//

import SwiftUI

// MARK: TabBar
struct TabBar: View {
    var body: some View {
        TabView {
            DiaryList().tabItem({
                Image(systemName: "book.fill")
                Text("Diary")
            })
            EditDiary().tabItem({
                Image(systemName: "calendar.badge.plus")
                Text("Today")})
        }
    }
}

struct TabBar_Previews: PreviewProvider {
    static var previews: some View {
        TabBar().environmentObject(Model.mock)
    }
}
