//
//  Model.swift
//  Diary
//
//  Created by ios on 12.10.19.
//  Copyright © 2019 TUM LS1. All rights reserved.
//

import Foundation
import Combine

// MARK: Model
public class Model {
    
    @Published public private(set) var diaries: [DiaryEntry] {
        didSet {
            diaries.saveToFile()
        }
    }
    
    // MARK: Initializers
    public init(diaries: [DiaryEntry]? = nil) {
        self.diaries = DiaryEntry.loadFromFile()
    }
    
    public func diary(_ id: DiaryEntry.ID) -> DiaryEntry? {
        for diary in diaries where diary.id == id {
                return diary
        }
        return nil
    }
    
    public func diary() -> DiaryEntry? {
        diaries.first {
            $0.date.noon == Date().noon
        }
    }
    
    public func delete(diary id: DiaryEntry.ID) {
        guard let diary = diary(id),
            let index = diaries.firstIndex(of: diary)
            else { return }
        diaries.remove(at: index)
    }
    
    public func save(_ diary: DiaryEntry) {
        delete(diary: diary.id)
        diaries.append(diary)
    }
}

extension Model: ObservableObject {}

extension Model {
    public static var mock: Model {
        let mock = Model(diaries: [
            DiaryEntry(mood: .sad, description: "..sad"),
            DiaryEntry(mood: .happy, description: "..happy")
        ])
        return mock
    }
}
