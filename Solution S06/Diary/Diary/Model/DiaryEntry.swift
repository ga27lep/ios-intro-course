//
//  DiaryEntry.swift
//  Diary
//
//  Created by ios on 12.10.19.
//  Copyright © 2019 TUM LS1. All rights reserved.
//

import SwiftUI

// MARK: DiaryEntry: an object to store properties of a diary
public struct DiaryEntry {
    
    public enum Mood: Int, CaseIterable, Identifiable, Codable {
        public var id: Int {
            self.rawValue
        }
        
        case happy = 0
        case sad = 1
        case itsOk = 2
        case relieved = 3
        case angry = 4
        
        init(value: Int) {
            switch value {
            case 0:
                self = .happy
            case 1:
                self = .sad
            case 2:
                self = .itsOk
            case 3:
                self = .relieved
            default:
                self = .angry
            }
        }
    }
    
    public var id: UUID?
    public var mood: Mood
    public var description: String
    public var date: Date
    
    var dateFormatted: String {
        let date = DateFormatter()
        date.timeStyle = .none
        date.dateStyle = .short
        return date.string(from: self.date)
    }
    
    var dayToString: String {
        return DiaryEntry.extractDayToString(diary: self)
    }
    
    var emojiMood: String {
        switch mood {
        case .happy:
            return "😁"
        case .angry:
            return "😡"
        case .itsOk:
            return "🙂"
        case .relieved:
            return "😌"
        case .sad:
            return "😢"
        }
    }
    
    var color: Color {
        switch mood {
        case .happy:
            return .green
        case .angry:
            return .red
        case .itsOk:
            return .yellow
        case .relieved:
            return .pink
        case .sad:
            return .gray
        }
    }
    
    public init(id: UUID? = nil, mood: Mood, description: String, date: Date? = nil) {
        
        self.id = id ?? UUID()
        self.mood = mood
        self.description = description
        self.date = date ?? Date()
    }
    
    static func extractDayToString(diary: DiaryEntry) -> String {
        let calendar = Calendar(identifier: .gregorian)
        let weekDay = calendar.component(.weekday, from: diary.date)
        switch weekDay {
        case 1:
            return "Monday"
        case 2:
            return "Tuesday"
        case 3:
            return "Wednesday"
        case 4:
            return "Thursday"
        case 5:
            return "Friday"
        case 6:
            return "Saturday"
        default:
            return "Sunday"
        }
    }
}

extension DiaryEntry: Identifiable { }

extension DiaryEntry: Equatable {
    public static func == (lhs: DiaryEntry, rhs: DiaryEntry) -> Bool {
        return lhs.id == rhs.id
    }
}

extension DiaryEntry: Comparable {
    public static func < (lhs: DiaryEntry, rhs: DiaryEntry) -> Bool {
        return lhs.date > rhs.date
    }
}

extension DiaryEntry: Codable { }

extension DiaryEntry: LocalFileStorable {
    static var fileName: String = "DiaryEntries"
}
