/*:
 # Swift 4
 ## Homework
 
 The goal of this homework is to create a linked list in Swift, work with `URLSession`, the `Result` type, and Futures and Promises.
 */
import Foundation
import Combine
/*:
 **Task 1:** Create a generic `Element` class with a value property of a generic type `T`. The element should represent an element in a linked list! Each `Element` should have optional `next` and `previous` properties of the type `Element` with the same generic type parameter. Create an initializer that has default values of `nil` for the `next` and `previous` properties (`init(_ value: T, next: Element<T>? = nil, previous: Element<T>? = nil)`).
 */
class Element<T> {
    var value: T
    var next: Element<T>?
    var previous: Element<T>?
    
    init(_ value: T, next: Element<T>? = nil, previous: Element<T>? = nil) {
        self.value = value
        self.next = nil
        self.previous = nil
    }
    
    func getValue() -> T? {
        return self.value
    }
    
    func insert(value: T) {
        return self.value = value
    }
    
    var description: String {
        var print = "["
        var elem = self
        while let next = elem.next {
            print += "\(elem.value), "
            elem = next
        }
        print += "\(elem.value)]"
        return print
    }
}
/*:
 **Task 2:** Create a `LinkedList` class with a generic constraint `T`. The `LinkedList` class should only store an optional reference to the first element in the `LinkedList`, the `head`.
 */
class LinkedList<T> {
    var head: Element<T>?
    var count: Int {
        var sum = 0
        var elem = head
        while elem != nil {
            sum += 1
            elem = elem?.next
        }
        return sum
    }
    
    init () { }
    
    func insert(_ value: T, at position: Int? = nil) {
        let newElement = Element<T>(value)
        if position == 0 {
            newElement.next = head
            head?.previous = newElement
            head = newElement
        } else {
            guard let position = position  else {
                newElement.next = head
                head?.previous = newElement
                head = newElement
                return
            }
            guard let previousElement = self.elem(at: position - 1) else {
                return
            }
            
            let nextElement = previousElement.next
            newElement.next = previousElement.next
            newElement.previous = previousElement
            nextElement?.previous = newElement
            previousElement.next = newElement
        }
    }
    
    public func elem(at position: Int) -> Element<T>? {
        if position == 0 {
            return head
        } else {
            guard let head = head else {
                return nil
            }
            var element = head.next
            for _ in 1..<position {
                element = element?.next
                if element == nil {
                    return nil
                }
            }
            return element
        }
    }
    
    func getValue(at position: Int) -> T? {
        if position == 0 {
            guard let head = head else {
                return nil
            }
            return head.getValue()
        } else {
            guard let head = head else {
                return nil
            }
            var elem = head.next
            for _ in 1..<position {
                elem = elem?.next
                if elem == nil {
                    return nil
                }
            }
            return elem?.getValue()
        }
    }
}


extension LinkedList: CustomStringConvertible where T: CustomStringConvertible {
    var description: String {
        guard let head = head else {
           return "[]"
        }
        return head.description
    }
}

let element = Element<Int>(4)
var list = LinkedList<Int>()
list.count
list.insert(2, at: 0)
list.count
list.insert(2, at: 1)
list
list.insert(3, at: 2)
list
list.getValue(at: 3)
list.insert(4, at: 3)
list
list.count
list.insert(5, at: 4)
list
list.count
list.getValue(at: 0)
list.getValue(at: 1)
list.getValue(at: 2)
list.getValue(at: 3)
list.getValue(at: 4)
list.getValue(at: 5)
list.description
/*:
 **Task 3:** Add an insert method to the `Element` and `LinkedList` that allows the user to insert a value of type `T` at an arbitrary position in the `LinkedList`. Think about a recursive approach (`func insert(_ value: T, at position: Int? = nil)`).
 */

/*:
 **Task 4:** Add a `getValue` method to the `Element` and `LinkedList` that returns a value of type `T?` at a position in the `LinkedList` based on the index (`Int`) passed into the method (`func getValue(at position: Int) -> T?`).
 */

/*:
 **Task 5:** Add a `count` property to `Element` and `LinkedList` that returns the number of elements in the LinkedList.
 */

/*:
 **Task 6:** Create an `Element` and `LinkedList` extension so they conform to `CustomStringConvertible` that offers a computed property named `description` when the element stored in the `LinkedList` or `Element` conforms to the `CustomStringConvertible` protocol. The content of the `description` should be exactly like the build description of Swift Arrays using squared brackets and comma separated values. Reference: "[-2,-1,0,1,2,3]".
 */

/*:
 **Task 7:** Create an instance of `LinkedList` that stores `Int` instances named `list`. Append multiple values at different indices and test your implementations of all functions/properties.
 */

/*:
 **Task 8:** Create a class named `UserManager` that stores a list of `User` objects (`users`). The `User` objects should be encodable and decodable to the JSON found at "https://jsonplaceholder.typicode.com/users". You only need to encode and decode the `id`, `name`, `username` and `email` properties, leave out everything else provided by "https://jsonplaceholder.typicode.com/users". The class should have a method called `loadUsers()` that loads the users from jsonplaceholder.typicode.com and stores them in a `users` array. Use combine `Publisher` and operators to get a list of all `User`s from "https://jsonplaceholder.typicode.com/users".
 */
struct User: Codable {
    let id: Int
    var name: String
    var username: String
    var email: String
}

class UserManager {
    var users: [User] = [] {
        didSet {
            users.forEach { print($0.name) }
        }
    }
    var publisher: AnyCancellable?
    let baseURL = "https://jsonplaceholder.typicode.com/users"
    let session = URLSession.shared
    
    func loadUsers() {
        guard let url = URL(string: baseURL) else {
            fatalError("Invalid URL!")
        }
        publisher = session.dataTaskPublisher(for: url)
            .map(\.data)
            .decode(type: [User].self, decoder: JSONDecoder())
            .replaceError(with: [])
            .sink(receiveValue: { users in
                self.users = users
            })
    }
}

UserManager().loadUsers()
/*:
 **Bonus:** Extend the `User` struct to store an array of `Album` (`albums`). Extend the `loadUsers()` method in the `UserManager` to chain a request to "https://jsonplaceholder.typicode.com/albums?userId=USERID" with `USERID` is beeing replaces with the actual `User` `id` property value to get all the albums of a single user. Cascade the call using `flatMap` and a separate `Publisher` to load the corresponding `Albums` per user.
 */

/*:
 # Congratulations!
 ## You completed your fourth Swift homework! 👩‍💻👨‍💻
 Please submit it to your tutor as described in the slides. They will almost certainly have comments, so be prepared to re-iterate based on their feedback.
 */
