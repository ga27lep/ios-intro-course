//
//  BeerTrack.swift
//  BeerTracker
//
//  Created by ios on 09.10.19.
//  Copyright © 2019 TUM LS1. All rights reserved.
//

import Foundation

// MARK: a struct where the information of the beer tracker is stored
struct BeerTrack: Identifiable {
    var id: UUID
    var amountOfBeers: Int
    var location: String
    var description: String
    var date: Date
    var friends: [Friend]
    
    // MARK: a computed property in order to make the date look nice
    var dateDecription: String {
        let date = DateFormatter()
        date.timeStyle = .none
        date.dateStyle = .short
        return date.string(from: self.date)
    }
    
    // MARK: a computed property in order to count the beers without doing it manually
    var showBeers: String {
        return String(repeating: "🍺", count: amountOfBeers)
    }
    
    public init (id: UUID? = nil, amountOfBeers: Int, description: String, location: String, date: Date? = nil, friends: [Friend] ) {
        self.id = id ?? UUID()
        self.amountOfBeers = amountOfBeers
        self.location = location
        self.description = description
        self.date = date ?? Date()
        self.friends = friends
    }
    
    // MARK: a mock object in order to show data in the preview
    static let mock: [BeerTrack] = [
        BeerTrack(amountOfBeers: 3, description: "Nice time at Oktoberfest", location: "Munich", friends: Friend.mock),
        BeerTrack(amountOfBeers: 3, description: "What a beautiful time", location: "Oktober", friends: Friend.mock),
        BeerTrack(amountOfBeers: 4, description: "So amazing", location: "Bar", friends: Friend.mock),
        BeerTrack(amountOfBeers: 4, description: "A night to remember", location: "Garching", friends: Friend.mock)
    ]
}
