//
//  Friend.swift
//  BeerTracker
//
//  Created by ios on 09.10.19.
//  Copyright © 2019 TUM LS1. All rights reserved.
//

import Foundation

// MARK: a struct where every information of a friend is stored
struct Friend: Identifiable {
    var id: UUID
    var firstName: String
    var lastName: String
    var beerCount: Int
    var isFavourite: Bool
    
    public init (id: UUID? = nil, firstName: String, lastName: String, beerCount: Int, isFavourite: Bool) {
        self.id = id ?? UUID()
        self.firstName = firstName
        self.lastName = lastName
        self.beerCount = beerCount
        self.isFavourite = isFavourite
    }
    
    // MARK: a mock object for the preview
    static let mock: [Friend] = [
        Friend(firstName: "Anton", lastName: "Gruber", beerCount: 4, isFavourite: true),
        Friend(firstName: "Alexandra", lastName: "Pierce", beerCount: 2, isFavourite: false)
    ]
}
