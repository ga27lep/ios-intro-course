//
//  BeerTrackCell.swift
//  BeerTracker
//
//  Created by ios on 09.10.19.
//  Copyright © 2019 TUM LS1. All rights reserved.
//

import SwiftUI

struct BeerTrackCell: View {
    var beerTrack: BeerTrack
    
    // MARK: a view of a single cell in the beer tracker where everything is aligned
    var body: some View {
        HStack(alignment: .top) {
            VStack(alignment: .leading, spacing: 6) {
                Text(beerTrack.location)
                    .font(Font.system(size: 22, weight: .bold))
                Text("\(beerTrack.showBeers)")
            }.font(Font.system(size: 16))
            Spacer()
            Text("\(beerTrack.location), \(beerTrack.dateDecription)")
                .font(Font.system(size: 14))
        }
    }
}

struct BeerTrackCell_Previews: PreviewProvider {
    static var previews: some View {
        BeerTrackCell(beerTrack: BeerTrack.mock[0])
    }
}
