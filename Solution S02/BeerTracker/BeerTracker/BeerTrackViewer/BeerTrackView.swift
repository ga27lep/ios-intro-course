//
//  BeerTrackView.swift
//  BeerTracker
//
//  Created by ios on 09.10.19.
//  Copyright © 2019 TUM LS1. All rights reserved.
//

import SwiftUI

struct BeerTrackView: View {
    var beerTrack: BeerTrack
    
    var body: some View {
        
        // MARK: a list with the friends that you went drinking
        VStack(alignment: .leading, spacing: 10) {
            Group {
                Text(beerTrack.description)
                    .font(Font.system(size: 30, weight: .bold))
                Text("On \(beerTrack.dateDecription) you went drinking with the following friends:")
            }.padding()
            List(beerTrack.friends) { friend in
                HStack(alignment: .top) {
                    friend.isFavourite ? Text("⭐️") : Text("")
                    Text("\(friend.firstName)")
                    Text("\(friend.lastName)")
                    Spacer()
                    Text("\(friend.beerCount)x🍺 ")
                        .font(Font.system(size: 18))
                }
            }
        }
    }
}

struct BeerTrackView_Previews: PreviewProvider {
    static var previews: some View {
        BeerTrackView(beerTrack: BeerTrack.mock[0])
    }
}
