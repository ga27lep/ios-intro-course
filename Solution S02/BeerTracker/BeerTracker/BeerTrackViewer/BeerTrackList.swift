//
//  BeerTrackList.swift
//  BeerTracker
//
//  Created by ios on 09.10.19.
//  Copyright © 2019 TUM LS1. All rights reserved.
//

import SwiftUI

struct BeerTrackList: View {
    var beerTracks: [BeerTrack]
    
    // MARK: main view of beer tracker showing where you went to drink, how much you drank and when
    var body: some View {
        NavigationView {
            
            List(beerTracks) { beerTrack in
                NavigationLink(destination: BeerTrackView(beerTrack: beerTrack) ) {
                    BeerTrackCell(beerTrack: beerTrack)
                }
            }
            .navigationBarTitle("BeerTracker")
        }
    }
    
    init() {
       beerTracks = BeerTrack.mock
    }
}

struct BeerTrackList_Previews: PreviewProvider {
    static var previews: some View {
        BeerTrackList()
    }
}
