/*:
 # Swift 3
 ## Homework
 
 We will create a travel agency which allows to book flights and overnight stays from its catalog. 🛫 🛏 🛬
 
 **Important:** In general, you should use higher order functions (reduce, map, filter etc.) in this exercise whenever possible! To keep it simple we will store dates as Strings.
 */
import Foundation
/*:
 We added these things to get you started. Check out the code, we will use it in the following tasks. We also added quite some code to the below tasks to make you type less. It's an exciting time to be alive!
 */
struct Customer {
    let fullName: String
    var passportNumber: String?
}

extension Customer: Equatable {
    static func == (lhs: Customer, rhs: Customer) -> Bool {
        return lhs.fullName == rhs.fullName
    }
}
/*:
 **Task 1:** Create a protocol named `Bookable` with two `get`-only properties: `price` of type `Double` and `identifier` of type `String`.
 
 **Note:** we are using a protocol because inheritance does not make too much sense: the objects *behave* similarly but they are not similar objects!
 */
protocol Bookable {
    var price: Double { get }
    var identifier: String { get }
}
/*:
 **Task 2:** Create two structs which implement the protocol:
 * `Flight` has an `airlineCode` of type `String` and a `flightNumber` of type `Int`. These two properties are combined to build its `identifier`.
 * `OvernightStay` has a property `hotelName` which is also used for its `identifier`.
 */
struct Flight: Bookable {
    let flightNumber: Int
    let airlineCode: String
    let price: Double
    
    var identifier: String {
        return "\(airlineCode)\(flightNumber)"
    }
}

struct OvernightStay: Bookable {
    let hotelName: String
    let price: Double
    
    var identifier: String {
        return "\(hotelName)"
    }
}
/*:
 **Task 3:** Take a look at the struct `Booking` below. The components of the booking are stored in a private Array that contains tuples of type `(item: Bookable, date: String)`. Now add a computed property `price` which sums up the price of all components of the booking and multiplies it with the number of customers. Factor in the discount here!
 */
struct Booking {
    let customers: [Customer]
    let discount: Double
    let id: Int
    private(set) var items: [(item: Bookable, date: String)]
    
    init(_ id: Int, customers: [Customer], items: [(Bookable, String)], discount: Double = 0.0) {
        self.id = id
        self.customers = customers
        self.items = items
        self.discount = discount
    }
    var price: Double {
        items.reduce(0.0) { $0 + $1.item.price } * Double(customers.count) * (1 - discount)
    }
}
/*:
 **Task 4:** Extend `Customer`, `Flight`, `OvernightStay` and `Booking` to conform to `CustomStringConvertible`. We already added the stubs for you. Make sure to have a nice, readable description to each!
 */
extension Customer: CustomStringConvertible {
    var description: String {
        "Full name: \(fullName), Passport number: \(passportNumber ?? "Unknown")"
    }
}

extension Flight: CustomStringConvertible {
    var description: String {
        return "Flight: \(identifier) Price: \(price)"
    }
}

extension OvernightStay: CustomStringConvertible {
    var description: String {
        return "Hotel: \(identifier) Price: \(price)"
    }
}

extension Booking: CustomStringConvertible {
    var description: String {
        let customersList = customers.map({ $0.fullName }).reduce("", { $0.isEmpty ? $0 + "\($1)" : $0 + " and \($1)" })
        let itemsList = items.map({ $0.item }).reduce("", { $0.isEmpty ? $0 + "\($1)" : $0 + " and \($1)" })
        
        return "\"The booking with the id: \(id) was bought by the customers: \(customersList) with a discount of \(discount). The discounted price is now: \(price) and the booked items are \(itemsList)\""
    }
}
/*:
 **Task 5:** This `struct` named `TravelCatalog` already has an array of `Bookable` items. Now it should also be possible to access an item with a `subscript` through its `identifier`.
 */
struct TravelCatalog {
    var items: [Bookable]
    
    subscript(itemId: String) -> Bookable? {
        items.first(where: { $0.identifier == itemId })
    }
}
/*:
 **Task 6:** We already prepared the class `TravelAgency` for you. Add a computed property which returns an array of `Customer`s based on the customers that made their bookings at the travel agency. Remember, use a higher order function wherever possible in this exercise!
 If you need some inspiration, take a look at the `flatMap(_:)` documentation: https://developer.apple.com/documentation/swift/sequence/2905332-flatmap
 */
class TravelAgency {
    private let catalog: TravelCatalog
    private var bookings: [Booking] = []
    
    init(catalog: TravelCatalog) {
        self.catalog = catalog
    }
    var clients: [Customer] {
        bookings.flatMap({ $0.customers })
    }
}
/*:
 **Task 7:** Create an enumeration conforming to the `Error` protocol named `TravelError` with the following cases: `noPassportAvailable(customers: [Customer])`, `noBookingFound(identifier: Int)`, `notOfferedItem(identifier: String)`, and `noCustomers`.
 */
enum TravelError: Error {
    case noPassportAvailable(customers: [Customer])
    case notOfferedItem(identifier: String)
    case noCustomers
    case noBookingFound(identifier: Int)
}
/*:
 **Task 8:** Add a method `add` of type `(Bookable, String) -> ()` that requires an instance that conforms to `Bookable` and the date as a `String` when the instance should be booked:
 * The method should add the `Bookable` instance to the booking (hint: use the `mutating` keyword).
 * Check if the booking has customers, otherwise throw the appropriate `TravelError`.
 * If the item is a `Flight`, check whether the customers have a passport number, otherwise throw the appropriate `TravelError`.
 */
extension Booking {
    
    mutating func add(newItem: Bookable, date: String) throws {
        if customers.isEmpty {
            throw TravelError.noCustomers
        }
        
        if newItem is Flight {
            let filterCustomers = customers.filter({ $0.passportNumber == nil })
            if !filterCustomers.isEmpty {
                throw TravelError.noPassportAvailable(customers: filterCustomers)
            }
        }
        items.append((newItem, date))
    }
}
/*:
 **Task 9:** Add the following three methods to the TravelAgency class:
 * A method named `addItem` of type `(Bookable, String, Int) -> ()` that can throw an error. It should take an instance conforming to the `Bookable` protocol, the date when the element should be added and the id of the `Booking` that should be mutated. The method should add an item to the booking with the corresponding id if it is present in the bookings catalog of the travel agency.
 * A method to list all items below a certain price in a log statement.
 * A method to get all the bookings of a customer.
 
 Each method should indicate that it has finished with an appropriate log statement. As always, use descriptive argument labels and higher order functions!
 */
extension TravelAgency {
    
    func addItem(item: Bookable, date: String, id: Int ) throws {
        guard let itemAvailability = catalog[item.identifier] else {
            throw TravelError.notOfferedItem(identifier: item.identifier)
        }
        for (index, element) in bookings.enumerated() where element.id == id {
            try bookings[index].add(newItem: itemAvailability, date: date)
            print("Requested item was added.")
            return
        }
        throw TravelError.noBookingFound(identifier: id)
    }
    
    func listItems(price: Double) {
        catalog.items.filter { $0.price <= price }.forEach { print($0) }
    }
    
    func getBookings(customer: Customer) -> [Booking] {
        return bookings.filter({ $0.customers.contains(where: { $0 == customer }) })
    }
}
/*:
 **Task 10:** Finally add a method `book` of type `([Customer], [(Bookable, String)], Double?) -> ()` to `TravelAgency`, which can throw an error.
 * The first argument should be the customers that want to book a trip, the second argument should be the bookings structured as in the `add` method and the last argument should be the discount.
 * The method should create a new `Booking` and add the given items (use the newly created `add` method).
 * Append the booking to the collection of bookings of the travel agency.
 * This method can throw an error (hint: `throws` keyword). Create a new `Booking`, append it to the `bookings` Array and use the `addItem` method created in the previous exercise to add the items to a newly created booking. Remember that you need to remove the new `Booking` in case of an error, and you need to propagate the error to the calling function.
 */
extension TravelAgency {
    private var nextId: Int {
        return bookings.reduce(0, { $0 > $1.id ? $0 : $1.id }) + 1
    }
    
    func book(newCustomers: [Customer], items: [(Bookable, String)], discount: Double?) throws {
        let booking = Booking(nextId, customers: newCustomers, items: [], discount: discount ?? 0)
        bookings.append(booking)
        do {
            try items.forEach { item in
                try addItem(item: item.0, date: item.1, id: booking.id)
            }
        } catch {
            bookings.removeAll(where: { $0.id == booking.id })
            throw error
        }
    }
}
/*:
 **Task 11:** Wrap everything up:
 * Create at least two flights, overnight stays and customers (one without passport number). Add the flights and overnight stays to a travel catalog and create a travel agency with that catalog.
 * List all items below a chosen price and try to book a flight with discount of 0.2 for both customers. Print your corresponding messages for all possible errors.
 * Add the missing passport number for the customer and try to book again. Add a helper function at the scope of the playground to reduce the amount of duplicated code for error handling.
 * Add an item of type `OvernightStay` to the newly created booking.
 * Print all bookings of the agency for one of the customers. Then print all customers of the agency.
 */

func book(agency: TravelAgency, customers: [Customer], items: [(Bookable, String)], discount: Double?) {
    do {
        try agency.book(newCustomers: customers, items: items, discount: discount)
        print("Booking completed!")
    } catch TravelError.noCustomers {
        print("The booking has no customer!")
    } catch TravelError.noPassportAvailable(_) {
        print("One of the customers does not have a passport number.")
    } catch let TravelError.notOfferedItem(identifier) {
        print("Sorry, item \(identifier) is not in our offer.")
    } catch let TravelError.noBookingFound(identifier) {
        print("Sorry, booking \(identifier) not found")
    } catch _ { }
}

let mucToIta = Flight(flightNumber: 1, airlineCode: "QWE12", price: 150)
let mucToFra = Flight(flightNumber: 2, airlineCode: "RTY34", price: 140)

let vacation = OvernightStay(hotelName: "SummerSunshine", price: 123.21)
let business = OvernightStay(hotelName: "ArenaHotel", price: 234.78)

var anton = Customer(fullName: "Anton Gruber", passportNumber: nil)
var alexa = Customer(fullName: "Alexandra Grieb", passportNumber: "16452522370")

let tourCatalog = TravelCatalog(items: [mucToIta, mucToFra, vacation, business])
let tour = TravelAgency(catalog: tourCatalog)

print(tour.listItems(price: 450))

book(agency: tour, customers: [anton, alexa], items: [(mucToIta, "30.10.2019"), (mucToFra, "29.10.2019")], discount: 0.2)
anton.passportNumber = "65446533391"
book(agency: tour, customers: [anton, alexa], items: [(mucToIta, "30.10.2019"), (mucToFra, "29.10.2019")], discount: 0.2)

//try tour.addItem(item: vacation, date: "29.10.2019", id: 0)

print(tour.getBookings(customer: anton))
print(tour.clients)
/*:
 # Congratulations!
 ## You completed your third Swift homework! 👩‍💻👨‍💻
 Please submit it to your tutor as described in the slides. They will almost certainly have comments, so be prepared to re-iterate based on their feedback.
 */
