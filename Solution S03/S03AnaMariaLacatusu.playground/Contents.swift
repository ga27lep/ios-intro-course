/*:
 # Swift 2
 ## Homework
 
 In this homework we will create a weather station which takes measurements and determines the weather accordingly. ☀️⛈
 
 
 **Task 1:** Create an enumeration called `MeasurementType` which can be `temperature` or `rainfall`.
 */
enum MeasurementType {
    case temperature
    case rainfall
}

enum Weather {
    case sunny
    case cloudy
    case rainy
}
/*:
 **Task 2:** Create a type named `City` that stores the `name` of a city and the `country` it is located in. City should conform to the `CustomStringConvertible` protocol using an extension.
 */
struct City {
    let name: String
    let country: String
}

extension City: CustomStringConvertible {
    var description: String {
        return "\(name), \(country)"
    }
}
/*:
 **Task 3:** Create a class `WeatherStation`.
 
 **Task 3.1:** The weather station has a private property called `measurements`. This property should be an Array of Tuples of type `(MeasurementType, Double)`. Add a property `city` of type `City` to the class. We should only have to pass an instance of type `City` to the constructor of `WeatherStation`, the measurements Array should be empty after initialization.
 
 **Task 3.2:** Add two ways of adding measurements:
 * One should be a method to add a single measurement using two separate arguments: a type (`MeasurementType`) and a value (`Double`). Use descriptive argument labels here!
 * The other should be a method that allows an arbitrary number of Tuples of type `(MeasurementType, Double)` to be added to the array. 
 
 Hint: use variadic parameters and the `append(contentsOf:)` method offered by `Array`
 
 **Task 3.3** Add two **computed properties** to your class:
 * The property `rainfall` should return the sum of values of all measurements which have the type `.rainfall`.
 * The property `meanTemperature` should return the mean of all measurements of type `.temperature` (return `nil` if there are no measurements, as otherwise you would be dividing by zero)
 * Use two different ways of iterating over the measurements array with a `for` loop: e.g. a `where` statement and pattern matching
 */
class WeatherStation {
    private var measurements: [(MeasurementType, Double)] = []
    let city: City
    
    init(city: City) {
        self.city = city
    }
    func addSingleMeasurement(type measurementType: MeasurementType, with value: Double) {
        self.measurements.append((measurementType, value))
    }
    
    func addMultipleMeasurements(measurements: (MeasurementType, Double)...) {
        self.measurements.append(contentsOf: measurements)
    }
    
    var rainfall: Double {
        var sumOfAllPrecipitations = 0.0
        
        for case let (.rainfall, precipitaton) in measurements {
            sumOfAllPrecipitations += precipitaton
        }
        
        return sumOfAllPrecipitations
    }
    
    var meanTemperature: Double? {
        var sumOfTemperatures = 0.0
        var counter = 0.0
        for (measurementType, precipitaton) in measurements where measurementType == .temperature {
            counter += 1.0
            sumOfTemperatures += precipitaton
        }
        return counter == 0 ? nil : sumOfTemperatures / counter
    }
    
    private func determineWeather() -> Weather? {
       guard !measurements.isEmpty, let meanTemperature = meanTemperature else {
            return nil
        }
        if rainfall > 10 {
            return .rainy
        }
        if meanTemperature > 20 {
            return .sunny
        }
        return .cloudy
    }
}

extension WeatherStation: CustomStringConvertible {
    var description: String {
        guard let weather = determineWeather(),
            let meanTemperature = meanTemperature else {
            return "Weather could not be determined"
        }
        switch weather {
        case .rainy:
            return "There is rain in \(city) today! There is a rainfall of \(rainfall) mm!"
        case .sunny:
            return "Look! It is so sunny today in \(city)! There is a temperature of \(meanTemperature) today"
        case .cloudy:
            return "You should take an umbrella from home. It is cloudy today in \(city). There is a temperature of \(meanTemperature) today"
        }
    }
}

extension WeatherStation: Equatable {
    
    static func == (lhs: WeatherStation, rhs: WeatherStation) -> Bool {
        return lhs.determineWeather() == rhs.determineWeather()
    }
    
    func compareWeather(to station: WeatherStation) {
        print("\(city) has \(self == station ? "" : "not") the same weather as \(station.city)!")
    }
}

var munich = WeatherStation(city: City(name: "Munich", country: "Germany"))
print(munich)
munich.addSingleMeasurement(type: .temperature, with: 10)
print(munich)
munich.addMultipleMeasurements(measurements: (measurementType: .temperature, value: 19),
                               (measurementType: .rainfall, value: 7),
                               (measurementType: .rainfall, value: 1))
var barcelona = WeatherStation(city: City(name: "Barcelona", country: "Spain"))
barcelona.addSingleMeasurement(type: .temperature, with: 25)
barcelona.addMultipleMeasurements(measurements: (measurementType: .temperature, value: 12),
                                  (measurementType: .rainfall, value: 2),
                                  (measurementType: .rainfall, value: 13))
munich.compareWeather(to: barcelona)
