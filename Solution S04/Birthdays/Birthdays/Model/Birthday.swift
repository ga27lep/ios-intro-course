//
//  Birthday.swift
//  Birthdays
//
//  Created by Paul Schmiedmayer on 10/09/19.
//  Copyright © 2019 TUM LS1. All rights reserved.
//

import Foundation

// MARK: an object which stores the properties of a birthday
struct Birthday {
    
    var id: UUID
    var date: Date
    var name: String
    var giftIdeas: String
    var giftDone: Bool
    
    var nextAge: Int {
        let ageComponents = Calendar.current.dateComponents([.year], from: date, to: Date())
        guard let age = ageComponents.year else {
            return -1
        }
        return age + 1
    }
    
    var formattedDate: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM"
        return dateFormatter.string(from: date)
    }
    
    public init(id: UUID? = nil, date: Date, name: String, giftIdeas: String? = nil, giftDone: Bool = false) {
        self.id = id ?? UUID()
        self.date = date
        self.name = name
        self.giftIdeas = giftIdeas ?? "No gift ideas yet"
        self.giftDone = giftDone
    }
    
    public init(id: UUID? = nil, year: Int, month: Int, day: Int, name: String, giftIdeas: String? = nil, giftDone: Bool = false) {
        self.id = id ?? UUID()
        self.date = Self.makeDate(year: year, month: month, day: day)
        self.name = name
        self.giftIdeas = giftIdeas ?? "No gift ideas yet"
        self.giftDone = giftDone
    }
}

extension Birthday {
    static func makeDate(year: Int, month: Int, day: Int) -> Date {
        let calendar = Calendar(identifier: .gregorian)
        let components = DateComponents(year: year, month: month, day: day)
        return calendar.date(from: components) ?? Date()
    }
}

extension Birthday: Identifiable { }
extension Birthday: Equatable { }

// MARK: a function for comparing two birdays by day and month
extension Birthday: Comparable {
    static func < (lhs: Birthday, rhs: Birthday) -> Bool {
        let calendar = Calendar.current
        let today = Date()
        
        guard var lhsOrdinality = calendar.ordinality(of: .day, in: .year, for: lhs.date),
            var rhsOrdinality = calendar.ordinality(of: .day, in: .year, for: rhs.date),
            let todayOrdinality = calendar.ordinality(of: .day, in: .year, for: today) else {
                return false
        }
        lhsOrdinality += lhsOrdinality <= todayOrdinality ? 365 : 0
        rhsOrdinality += rhsOrdinality <= todayOrdinality ? 365 : 0
        return lhsOrdinality < rhsOrdinality
    }
}
