//
//  Model.swift
//  Birthdays
//
//  Created by ios on 10.10.19.
//  Copyright © 2019 TUM LS1. All rights reserved.
//

import Foundation

// MARK: a class in order to work with mock data; functions to add, delete and save included
class Model {
    @Published public private(set) var birthdays: [Birthday]
    
    public init(birthdays: [Birthday]) {
        self.birthdays = birthdays
        self.birthdays.sort()
    }
    
    public func getBirthday (_ id: Birthday.ID?) -> Birthday? {
        for birthday in birthdays where birthday.id == id {
            return birthday
        }
        return nil
    }
    
    public func delete (birthday id: Birthday.ID) {
        guard let birthday = getBirthday(id), let index = birthdays.firstIndex(of: birthday) else {
            return
        }
        birthdays.remove(at: index)
        self.birthdays.sort()
    }
    
    public func save(_ birthday: Birthday) {
        delete(birthday: birthday.id)
        birthdays.append(birthday)
        birthdays.sort()
    }
}

extension Model: ObservableObject { }

extension Model {
    public static var mock: Model {
        let mock = Model(birthdays: [
            Birthday(year: 1990, month: 1, day: 11, name: "Dora Dzvonyar", giftIdeas: "A unicorn that sparkles"),
            Birthday(year: 1995, month: 12, day: 29, name: "Paul Schmiedmayer", giftIdeas: "A big hug", giftDone: true),
            Birthday(year: 1991, month: 2, day: 18, name: "Dominic Henze", giftIdeas: "Ice ceam, lots of ice cream", giftDone: true),
            Birthday(year: 1996, month: 10, day: 10, name: "Florian Bodlée")
        ])
        return mock
    }
}
