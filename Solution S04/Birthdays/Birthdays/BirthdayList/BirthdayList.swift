//
//  BirthdayList.swift
//  Birthdays
//
//  Created by ios on 11.10.19.
//  Copyright © 2019 TUM LS1. All rights reserved.
//

import SwiftUI

// MARK: here you can see all the birthdays listed
struct BirthdayList: View {
    
    @EnvironmentObject private(set) var model: Model
    private var birthdays: [Birthday] {
        model.birthdays
    }
    @State var presentAddBirthday = false
    
    var body: some View {
        NavigationView {
            List {
                ForEach(birthdays) { birthday in
                    NavigationLink(destination: EditBirthday(id: birthday.id)) {
                        BirthdayCell(birthday: birthday)
                    }
                }.onDelete(perform: delete)
            }.navigationBarTitle(Text("Upcoming Birthdays"), displayMode: .large)
                .navigationBarItems(trailing: addButton)
                .sheet(isPresented: $presentAddBirthday) {
                    NavigationView {
                        EditBirthday()
                    }.environmentObject(self.model)
            }
        }.onAppear(perform: { })
    }
    
    func delete(offsets: IndexSet) {
        offsets.map { model.birthdays[$0].id }.forEach { model.delete(birthday: $0) }
    }
    
    private var addButton: some View {
        Button(action: { self.presentAddBirthday = true }) {
            Image(systemName: "plus")
        }
    }
}

struct BirthdayList_Previews: PreviewProvider {
    static var previews: some View {
        BirthdayList()
            .environmentObject(Model.mock)
    }
}
