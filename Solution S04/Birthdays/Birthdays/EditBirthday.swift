//
//  EditBirthday.swift
//  Birthdays
//
//  Created by ios on 10.10.19.
//  Copyright © 2019 TUM LS1. All rights reserved.
//

import SwiftUI

// MARK: here you can edit or add a new birthday and perform save and delete actions
struct EditBirthday: View {
    @EnvironmentObject private var model: Model
    
    @State var id: Birthday.ID?
    @State private var date = Date()
    @State private var name = ""
    @State private var giftIdeas = ""
    @State private var giftDone = false
    @State private var showDeleteAlert = false
    @Environment(\.presentationMode) private var presentationMode
    
    var body: some View {
        Form {
            Section(header: Text("New Person")) {
                TextField("Enter name", text: $name)
            }
            Section(header: Text("Gift Ideas")) {
                TextField("Any gift ideas?", text: $giftIdeas)
            }
            Section(header: Text("Bought gift?")) {
                Picker("Bought gift", selection: $giftDone) {
                    Text("Yes").tag(true)
                    Text("No").tag(false)
                }.pickerStyle(SegmentedPickerStyle())
            }
            Section() {
                DatePicker(selection: $date, in: ...Date(), displayedComponents: [.date]) {
                    Text("Date")
                }
            }
        }.onAppear(perform: updateStates)
            .navigationBarTitle(id == nil ? "New Birthday" : "Edit Birthday", displayMode: .inline)
            .navigationBarItems(trailing: saveButton)
    }
    
    private func updateStates() {
        guard let birthday = model.getBirthday(id) else {
            name = ""
            giftIdeas = ""
            giftDone = false
            date = Date()
            return
        }
        
        id = birthday.id
        name = birthday.name
        giftIdeas = birthday.giftIdeas
        giftDone = birthday.giftDone
        date = birthday.date
    }
    
    private var saveButton: some View {
        Button(action: save) {
            Text("Save").bold()
        }
    }
    
    private func save() {
        let birthday = Birthday(id: id, date: date, name: name, giftIdeas: giftIdeas, giftDone: giftDone)
        model.save(birthday)
        updateStates()
        
        presentationMode.wrappedValue.dismiss()
    }
}

struct EditBirthday_Previews: PreviewProvider {
    static let model = Model.mock
    static var previews: some View {
        NavigationView {
            EditBirthday()
                .navigationBarTitle("Edit Birthday")
        }.environmentObject(model)
    }
}
